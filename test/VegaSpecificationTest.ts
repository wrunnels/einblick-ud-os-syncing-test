import { expect, assert } from "chai";
import "mocha";
import * as fs from "fs";
import { TestUtils } from "./TestUtils";

const operatorDir = TestUtils.OPERATOR_DIR;
const visualizationFile = TestUtils.VISUALIZATION_FILE;
const operatorSpecFile = TestUtils.SPECIFICATION_FILE;

describe("VisualizationSpecificationChecker", () => {
    before((done) => {
        done();
    });
    beforeEach((done) => {
        // do nothing
        done();
    });

    describe("Ensure all visualization specifications are parseable", () => {
        it("should find no nonconformant specs", (done) => {
            let invalidSpecs = 0;

            const udoDirs = fs.readdirSync(operatorDir).filter((dirName) => fs.lstatSync(operatorDir + dirName).isDirectory());
            udoDirs.forEach((dir) => {
                const fullDirName = operatorDir + dir + "/";
                const fullOperatorSpecFileName = `${fullDirName}${operatorSpecFile}`;
                const fullVisualizationSpecFileName = `${fullDirName}${visualizationFile}`;
                if (
                    fs.readdirSync(fullDirName).includes(visualizationFile) &&
                    fs.readdirSync(fullDirName).includes(operatorSpecFile) &&
                    fs.lstatSync(fullOperatorSpecFileName).isFile() &&
                    fs.lstatSync(fullVisualizationSpecFileName).isFile()
                ) {
                    const operatorSpec = fs.readFileSync(fullOperatorSpecFileName).toString();
                    const visualizationSpec = fs.readFileSync(fullVisualizationSpecFileName).toString();
                    if (visualizationSpec.trim()) {
                        try {
                            const parsedOperatorSpec = JSON.parse(operatorSpec.trim());
                            const paramKeys = parsedOperatorSpec
                                ? Object.keys(parsedOperatorSpec.ComputationDescription.ValueInputDescription ?? {})
                                : [];
                            let processedText = visualizationSpec;
                            paramKeys.forEach((paramKey) => {
                                processedText = processedText.split(`params['${paramKey}']`).join("42");
                            });
                            processedText = processedText.split("container_width").join("1");
                            processedText = processedText.split("container_height").join("1");
                            return JSON.parse(processedText);
                        } catch (err) {
                            invalidSpecs += 1;
                            console.log("Found invalid spec:", fullVisualizationSpecFileName, "----- details:");
                            console.error(err);
                        }
                    }
                }
            });

            if (invalidSpecs > 0) {
                console.log(`\n----- Found ${invalidSpecs} invalid visualization specification${invalidSpecs !== 1 ? "s" : ""} -----\n`);
            }

            expect(invalidSpecs).to.equal(0);
            done();
        });
    });
});
