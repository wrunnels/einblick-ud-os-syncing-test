import { expect, assert } from "chai";
import "mocha";
import * as fs from "fs";
import { TestUtils } from "./TestUtils";

const operatorDir = TestUtils.OPERATOR_DIR;
const validFileNames = TestUtils.VALID_FILE_NAMES;

describe("FileNameChecker", () => {
    before((done) => {
        done();
    });
    beforeEach((done) => {
        // do nothing
        done();
    });

    describe("Ensure all file names conform to the standard", () => {
        it("should find no invalid files or directories", (done) => {
            let improperDirs = 0;
            let invalidFileNames = 0;

            const udoDirs = fs.readdirSync(operatorDir).filter((dirName) => fs.lstatSync(operatorDir + dirName).isDirectory());
            udoDirs.forEach((dir) => {
                const fullDirName = operatorDir + dir + "/";
                const fileNames = fs.readdirSync(fullDirName);
                for (let fileName of fileNames) {
                    const fullFileName = `${fullDirName}${fileName}`;
                    if (fs.lstatSync(fullFileName).isDirectory()) {
                        console.log("Found invalid directory:", dir + "/" + fileName);
                        improperDirs += 1;
                    } else {
                        if (!validFileNames.includes(fileName)) {
                            console.log("Found invalid file:", dir + "/" + fileName);
                            invalidFileNames += 1;
                        }
                    }
                }
            });

            if (improperDirs > 0 || invalidFileNames > 0) {
                console.log(
                    `\n----- Found ${improperDirs} invalid director${
                        improperDirs !== 1 ? "ies" : "y"
                    } and ${invalidFileNames} invalid file${invalidFileNames !== 1 ? "s" : ""} -----\n`
                );
            }

            expect(improperDirs).to.equal(0);
            expect(invalidFileNames).to.equal(0);

            done();
        });
    });
});
