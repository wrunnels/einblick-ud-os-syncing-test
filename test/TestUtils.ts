import { UserOperatorType } from "../montana-api/src/enums/UserOperatorType";

export class TestUtils {
    public static OPERATOR_DIR = "./operators/";

    public static TYPES_FILE = "operator_type.json";
    public static SPECIFICATION_FILE = "operator_description.json";
    public static REQUIREMENTS_FILE = "requirements.txt";
    public static MODEL_DEFINITION_FILE = "model_definition.py";
    public static OPEN_FILE = "on_open.py";
    public static BATCH_FILE = "on_batch.py";
    public static RESET_FILE = "on_reset.py";
    public static CLOSE_FILE = "on_close.py";
    public static VISUALIZATION_FILE = "visualization.json";
    public static FILTERS_FILE = "filters.json";

    public static VALID_FILE_NAMES = [
        TestUtils.TYPES_FILE,
        TestUtils.SPECIFICATION_FILE,
        TestUtils.REQUIREMENTS_FILE,
        TestUtils.MODEL_DEFINITION_FILE,
        TestUtils.OPEN_FILE,
        TestUtils.BATCH_FILE,
        TestUtils.RESET_FILE,
        TestUtils.CLOSE_FILE,
        TestUtils.VISUALIZATION_FILE,
        TestUtils.FILTERS_FILE
    ];

    public static VALID_TYPES = Object.values(UserOperatorType);

    constructor() {}
}
