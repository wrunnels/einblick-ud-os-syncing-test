import { expect, assert } from "chai";
import "mocha";
import * as fs from "fs";
import { OperatorUtil } from "../montana-api/src/utils/OperatorUtil";
import { UserOperator } from "../montana-api/src/index";
import { TestUtils } from "./TestUtils";

const operatorDir = TestUtils.OPERATOR_DIR;
const operatorSpecificationFile = TestUtils.SPECIFICATION_FILE;

describe("OperatorSpecificationChecker", () => {
    before((done) => {
        done();
    });
    beforeEach((done) => {
        // do nothing
        done();
    });

    describe("Ensure all operator specifications conform to the montana-api standard", () => {
        it("should find no nonconformant specs", (done) => {
            let invalidSpecs = 0;

            const udoDirs = fs.readdirSync(operatorDir).filter((dirName) => fs.lstatSync(operatorDir + dirName).isDirectory());
            udoDirs.forEach((dir) => {
                const fullDirName = operatorDir + dir + "/";
                const fullFileName = `${fullDirName}${operatorSpecificationFile}`;
                if (!fs.readdirSync(fullDirName).includes(operatorSpecificationFile)) {
                    invalidSpecs += 1;
                    console.log("Could not find spec at location:", fullFileName);
                }
                if (fs.lstatSync(fullFileName).isFile()) {
                    const operatorSpec = fs.readFileSync(fullFileName).toString();
                    try {
                        OperatorUtil.GetUserOperatorDescriptionFromCode(
                            operatorSpec,
                            UserOperator.create({ DisplayName: "test", OperatorType: "test" })
                        );
                    } catch (err) {
                        invalidSpecs += 1;
                        console.log("Found invalid spec:", fullFileName, "----- details:");
                        console.error(err);
                    }
                }
            });

            if (invalidSpecs > 0) {
                console.log(`\n----- Found ${invalidSpecs} invalid operator specification${invalidSpecs !== 1 ? "s" : ""} -----\n`);
            }

            expect(invalidSpecs).to.equal(0);
            done();
        });
    });
});
