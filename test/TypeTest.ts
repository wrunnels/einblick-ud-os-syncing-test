import { expect, assert } from "chai";
import "mocha";
import * as fs from "fs";
import { TestUtils } from "./TestUtils";

const operatorDir = TestUtils.OPERATOR_DIR;
const typesFile = TestUtils.TYPES_FILE;
const validTypes = TestUtils.VALID_TYPES;

describe("TypeChecker", () => {
    before((done) => {
        done();
    });
    beforeEach((done) => {
        // do nothing
        done();
    });

    describe("Ensure all UDOs have Type and ModelType specified", () => {
        it("should find no UDOs without specified types", (done) => {
            let invalidUDOs = 0;

            const udoDirs = fs.readdirSync(operatorDir).filter((dirName) => fs.lstatSync(operatorDir + dirName).isDirectory());
            udoDirs.forEach((dir) => {
                const fullDirName = operatorDir + dir + "/";
                const fullTypesFileName = `${fullDirName}${typesFile}`;
                if (fs.readdirSync(fullDirName).includes(typesFile) && fs.lstatSync(fullTypesFileName).isFile()) {
                    const types = fs.readFileSync(fullTypesFileName).toString();
                    if (types.trim()) {
                        try {
                            const parsedOperatorSpec = JSON.parse(types.trim());
                            const type = parsedOperatorSpec["Type"];
                            const modelType = parsedOperatorSpec["ModelType"];
                            if (!(type && modelType && validTypes.includes(type) && validTypes.includes(modelType))) {
                                invalidUDOs += 1;
                                console.log("Found invalid type in file:", fullTypesFileName);
                            }
                        } catch (err) {
                            invalidUDOs += 1;
                            console.log("Found invalid types file:", fullTypesFileName, "----- details:");
                            console.error(err);
                        }
                    } else {
                        console.log("Found empty types file:", fullTypesFileName);
                        invalidUDOs += 1;
                    }
                } else {
                    console.log("No types file found for UDO at:", fullDirName);
                    invalidUDOs += 1;
                }
            });

            if (invalidUDOs > 0) {
                console.log(`\n----- Found ${invalidUDOs} invalid types file${invalidUDOs !== 1 ? "s" : ""} -----\n`);
            }

            expect(invalidUDOs).to.equal(0);
            done();
        });
    });
});
