batch_bottom = df.nsmallest(int(params["number"]), attributes["target"][0]) 

batch_numeric = batch_bottom.select_dtypes(include="number")
# For columns with all NAs or that have multiple types (e.g. str + boolean), convert everything to str to avoid errors
batch_object = batch_bottom.select_dtypes(include="object").applymap(lambda x: str(x))
batch_other = batch_bottom.select_dtypes(exclude=["number", "object"]).applymap(lambda x: str(x))

batch_result = pd.concat([batch_numeric, batch_object, batch_other], axis=1).reindex(columns=df.columns)

self.bottom = pd.concat([self.bottom, batch_result], axis=0).nsmallest(int(params["number"]), attributes["target"][0]) if self.bottom is not None else batch_result
return self.bottom