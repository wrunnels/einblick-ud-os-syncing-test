# print(df)
# print(params)
# print(attributes)

# print("haha")

import numpy as np 

new_df = df.copy()
idx = attributes["indices"]
idx_str = '__' + (', '.join(idx))
new_df[idx_str] = list(zip(*tuple(df[_] for _ in idx)))
new_df["__True"] = [True for _ in range(len(new_df.index))]
ans_df = new_df.pivot_table(
    index=idx_str, 
    columns=attributes["column"][0], 
    values=attributes["value"][0] if len(attributes["value"]) else "__True", 
    aggfunc = {'avg':np.mean, 'min':np.min, 'max':np.max}[params['aggfunc']])
# print(new_df[attributes["column"][0]])
freq = dict()
if params['distinct']:
    for i in new_df.index:
        e = new_df[attributes["column"][0]][i]
        uid = new_df[idx_str][i]
        if e not in freq:
            freq[e] = set()
        freq[e].add(uid)
    freq = {e: len(freq[e]) for e in freq}
else:
    for e in new_df[attributes["column"][0]]:
        freq[e] = (freq[e] if e in freq else 0) + 1
# print(freq)
sorted_cols = sorted(ans_df.columns, key=lambda x:freq[x], reverse=True)
# print(sorted_cols)
ans_df['pivot'] = [str(_[0] if len(_)==1 else _) for _ in ans_df.index.array]
# print(sorted_cols[:int(len(sorted_cols)*params['rtsig'])])
ans_df = ans_df[['pivot'] + sorted_cols[:int(len(sorted_cols)*params['rtsig'])]]
return ans_df
