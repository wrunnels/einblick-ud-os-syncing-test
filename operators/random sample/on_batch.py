n = None
frac = None
try: 
    n = int(params["number"]) 
    if n <= 0:
        n = None
        frac = float(params["fraction"])
except ValueError:
    try:
        frac = float(params["fraction"])
    except ValueError:
        raise ValueError("Must specify either a number of rows or fraction to sample")

replace = params["replacement"]
axis = int(params["sampling_axis"])

return df.sample(n=n, frac=frac, replace=replace, axis=axis, random_state=int(params["seed"]))