from sklearn.decomposition import PCA
import numpy as np

processed = df[attributes["features"]].select_dtypes(np.number)
pca = PCA(n_components=params["n_components"])
pca.fit(processed)
processed = pca.transform(processed)
return pd.concat([df, pd.DataFrame(processed, columns=["pca component " + str(i+1) for i in range(params["n_components"])])], axis=1)