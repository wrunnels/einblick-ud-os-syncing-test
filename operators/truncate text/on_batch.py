try:
    int(params["number"])
except Exception:
    raise ValueError("Must specify an integer as maximum length")

for feature in [f for f in df.select_dtypes(include="object").columns if f in attributes["features"]]:
    df[feature] = df[feature].apply(lambda x: x[:int(params["number"])])