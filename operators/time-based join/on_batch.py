# print(dfs[0])
# # print(dfs)
# print(params)
# print(attributes)
# print(attributes['ptcol'])
# return dfs[0]

import numpy as np 

new_df = dfs[0].copy()
first_time_str = attributes['ptcol'][0]
new_df['__parsed_datetime'] = pd.to_datetime(new_df[first_time_str])
new_df = new_df.sort_values(by=['__parsed_datetime'], ignore_index=True)

rdf = dfs[1].copy()
second_time_str = attributes['stcol'][0]
rdf_cols_no_time = [_ for _ in rdf.columns]; rdf_cols_no_time.remove(second_time_str)
rdf['__parsed_datetime'] = pd.to_datetime(rdf[second_time_str])
rdf = rdf.sort_values(by=['__parsed_datetime'], ignore_index=True)

for c in rdf_cols_no_time:
    new_df[c] = None
ptr = 0
for i in range(len(new_df.index)):
    while not (ptr >= len(rdf.index) or rdf['__parsed_datetime'][ptr] > new_df['__parsed_datetime'][i]):
        ptr += 1
    for c in rdf_cols_no_time:
        new_df.loc[i, c] = (np.nan if ptr == 0 else rdf.loc[ptr-1, c])
new_df = new_df.drop(columns=['__parsed_datetime'])
return new_df
