if params["leading"] and params["trailing"]:
    strip_f = lambda x: x.strip()
elif params["leading"]:
    strip_f = lambda x: x.lstrip()
elif params["trailing"]:
    strip_f = lambda x: x.rstrip()
else:
    raise Exception("Must specify removing at least one of leading and trailing whitespace")

for feature in [f for f in df.select_dtypes(include="object").columns if f in attributes["features"]]:
    df[feature] = df[feature].apply(strip_f)