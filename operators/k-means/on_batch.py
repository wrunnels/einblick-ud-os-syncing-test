from sklearn.cluster import MiniBatchKMeans
from sklearn.preprocessing import normalize
import numpy as np

self._batch_num += 1
if self._batch_num == 1:
    self._num_clusters = params["num_clusters"]
    self._kmeans = MiniBatchKMeans(n_clusters=self._num_clusters)

processed = normalize(df[attributes["features"]].select_dtypes(np.number))

self._kmeans.partial_fit(processed)
df["cluster"] = self._kmeans.predict(processed) + 1

return df