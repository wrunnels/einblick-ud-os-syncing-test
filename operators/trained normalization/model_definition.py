from copy import deepcopy
import numpy as np

class MLPrimitiveUDFOperator:
    """UDF operator for executing ML primitive."""

    def __init__(self, primitive):
        self._primitive = primitive

    def process_batch(self, batches, **kwargs) -> pd.DataFrame:
        """Execute the primitive over the data."""
        if batches and batches[0].empty:
            raise Exception("reached this condition in process_batch")
        
        return self._primitive.transform(batches, **kwargs)

class NormalizationUDFPrimitive:
    """Normalization primitive for UDF."""

    def __init__(self, primitive):
        self._primitive = primitive

    def transform(self, inputs, **kwargs):
        return pd.DataFrame(self._primitive.predict(inputs[0]))


class Normalizer:
    def __init__(self, feature: str):
        self._feature = feature
        self._min = None
        self._max = None
    
    def normalize(self, s: pd.Series):
        return (s - self._min)/(self._max - self._min)
    
    def columnExists(self, df):
        if self._feature not in df:
            raise AttributeError("Dataframe does not have column " + self._feature)

    def predict(self, df):
        self.columnExists(df)
        if self._min is None or self._max is None:
            raise AttributeError("Model has not been trained")
        
        augmented = df.copy()
        augmented[self._feature + "_normalized"] = self.normalize(df[self._feature])
        return augmented
    
    def transform(self, df):
        return df
    
    def partial_fit(self, df):
        self.columnExists(df)
        col = df[self._feature]
        min_v = col.min()
        max_v = col.max()
        if self._min is None or self._min > min_v:
            self._min = min_v
        if self._max is None or self._max < max_v:
            self._max = max_v