self._normalizer.partial_fit(df)

return MLPrimitiveUDFOperator(NormalizationUDFPrimitive(deepcopy(self._normalizer)))