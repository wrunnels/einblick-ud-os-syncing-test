from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import pandas.api.types as ptypes


class MLPrimitiveUDFOperator:
    """UDF operator for executing ML primitive."""

    def __init__(self, primitive):
        self._primitive = primitive

    def process_batch(self, batches, **kwargs) -> pd.DataFrame:
        """Execute the primitive over the data."""
        if batches and batches[0].empty:
            raise Exception("reached this condition in process_batch")
        
        return self._primitive.transform(batches, **kwargs)


class WordExtractor:
    """ Word Extractor based on TFIDF"""

    def __init__(self, max_df, min_df, max_features, stop_words, ngram_range, target, extractor_type):
        self._target = target
        if extractor_type == "1":  # tfidf
            self._vectorizer = TfidfVectorizer( max_df= max_df,
                                                min_df=min_df,
                                                max_features=max_features,
                                                stop_words=stop_words,
                                                ngram_range=ngram_range)
        else:  # count
            self._vectorizer = CountVectorizer( max_df= max_df,
                                                min_df=min_df,
                                                max_features=max_features,
                                                stop_words=stop_words,
                                                ngram_range=ngram_range)            


    def fit(self, batches):
        df = batches
        assert ptypes.is_string_dtype(df[self._target]), "Target column data-type is not str ({typ}). Please select another target column.".format(typ=df[self._target].dtype)
        s = df[self._target].tolist()
        self._vectorizer.fit(s)

    def transform(self, batches):
        df = batches[0]
        assert ptypes.is_string_dtype(df[self._target]), "Target column data-type is not str ({typ}). Please select another target column.".format(typ=df[self._target].dtype)
        s = df[self._target].tolist()
        vector = self._vectorizer.transform(s).todense()
        vector_df = pd.DataFrame(vector, columns = self._vectorizer.get_feature_names())

        df[self._vectorizer.get_feature_names()] = vector_df.values
        return df

