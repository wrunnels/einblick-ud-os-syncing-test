from copy import deepcopy

self._batch_num += 1
limit = 50000

if not self._stop: 
    if self._batch_num == 1:
        self._df = deepcopy(df)
    elif len(self._df) < limit:
        self._df = self._df.append(df)

    if len(self._df) >= limit:
        self._df = self._df[:limit]
        self._stop = True


    self._vectorizer = WordExtractor(max_df=0.95,
                                min_df=2,
                                max_features=params["max_words"],
                                stop_words='english',
                                ngram_range=(1, params["max_ngram"]),
                                target=attributes["target"][0],
                                extractor_type=params["extractor_type"])

    self._vectorizer.fit(self._df) 

return MLPrimitiveUDFOperator(deepcopy(self._vectorizer))