from copy import deepcopy
from sklearn.cluster import MiniBatchKMeans
# from sklearn.preprocessing import normalize
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import numpy as np

X = df[attributes["features"]].select_dtypes(np.number)
n_components = params["n_components"]

scaler = StandardScaler()
scaler.fit(X)
X=scaler.transform(X)   

pca = PCA(n_components=n_components)
pca.fit(X)
X = pca.transform(X)
pca_output = pd.concat([df, pd.DataFrame(X, columns=["pca component " + str(i+1) for i in range(n_components)])], axis=1)

self._batch_num += 1
if self._batch_num == 1:
    self._num_clusters = params["n_clusters"]
    self._kmeans = MiniBatchKMeans(n_clusters=self._num_clusters)

# normalized = normalize(pca_output[["pca component 1", "pca component 2"]])
pca_components = pca_output[["pca component " + str(i+1) for i in range(n_components)]]

self._kmeans.partial_fit(pca_components)
pca_output["cluster"] = self._kmeans.predict(pca_components) + 1

pca_biplot_coordinates = np.transpose(pca.components_[0:2, :])
pca_output["feature_name"] = ""

for i in range(pca_biplot_coordinates.shape[0]):
    new_row = pca_output.iloc[0]
    new_row.at["pca component 1"] = pca_biplot_coordinates[i, 0]
    new_row.at["pca component 2"] = pca_biplot_coordinates[i, 1]
    new_row.at["cluster"] = -1
    new_row.at["feature_name"] = attributes["features"][i]
    pca_output = pca_output.append(new_row, ignore_index=True)

print(pca_output)


return pca_output