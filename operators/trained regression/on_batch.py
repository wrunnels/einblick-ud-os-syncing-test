features = attributes['features']
target = attributes['target'][0]

X = df[features].drop(target, axis=1, errors="ignore")
y = df[target]

model = LinearRegressor(features, target)
model.fit(X, y)

return model