import numpy as np
from sklearn.linear_model import LinearRegression

class LinearRegressor:
    def __init__(self, features: str, target: str):        
        self._features = features
        self._target = target
        self._model = LinearRegression()

    def fit(self, X, y):
        self._model.fit(X, y)

    def process_batch(self, batches, **kwargs) -> pd.DataFrame:
        if batches and batches[0].empty:
            raise Exception("Input data is empty")            
        
        X = batches[0][self._features]

        predicted_target = pd.DataFrame(self._model.predict(X), columns=["predicted_" + self._target])
        return pd.concat([batches[0], predicted_target], axis=1)