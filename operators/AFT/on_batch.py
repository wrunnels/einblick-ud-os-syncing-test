import numpy as np
import random
from copy import deepcopy

def reservoir_sample(df):
    for _, row in df.iterrows():
        self._counter += 1
        if self._counter <= self._reservoir_size:
            self._reservoir.append(row)
        else:
            m = random.randint(0,self._reservoir_size)
            if m < self._reservoir_size:
                self._reservoir[m] = row
    return pd.DataFrame(self._reservoir)

df = reservoir_sample(df)

return df

self._model = SurvivalModel(self._target)
self._model.fit(df)

return MLPrimitiveUDFOperator(deepcopy(self._model))