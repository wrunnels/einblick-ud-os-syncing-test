import pandas.api.types as ptypes
import xgboost as xgb


class MLPrimitiveUDFOperator:
    """UDF operator for executing ML primitive."""

    def __init__(self, primitive):
        self._primitive = primitive

    def process_batch(self, batches, **kwargs) -> pd.DataFrame:
        """Execute the primitive over the data."""
        if batches and batches[0].empty:
            raise Exception("reached this condition in process_batch")
        
        return self._primitive.transform(batches, **kwargs)


class SurvivalModel:
    """Survial Modeling"""

    def __init__(self, target):
        self._target = target
        self._bst = None

    def fit(self, batches):
        y_train = batches[self._target].values
        X_train = batches.drop(columns=[self._target])

        y_lower_bound = y_train
        y_upper_bound = y_train
        dtrain = xgb.DMatrix(X_train.values)

        dtrain.set_float_info('label_lower_bound', y_lower_bound)
        dtrain.set_float_info('label_upper_bound', y_upper_bound)

        params = {'objective': 'survival:aft',
                'eval_metric': 'aft-nloglik',
                'aft_loss_distribution': 'normal',
                'aft_loss_distribution_scale': 1.20,
                'tree_method': 'hist', 'learning_rate': 0.1, 'max_depth': 200}
        print(X_train.shape, flush=True)
        self._bst = xgb.train(params, dtrain, num_boost_round=200, evals=[(dtrain, 'train')])

    def transform(self, batches):
        contains_target = False
        X_test = batches[0]
        if self._target in X_test.columns:
            contains_target = True
            y_test = X_test[self._target].values
            X_test = X_test.drop(columns=[self._target])
        dtest = xgb.DMatrix(X_test.values)
        X_test["predicted_" + self._target] = self._bst.predict(dtest)
        if contains_target:
            X_test[self._target] = y_test
        return X_test


