from sklearn.preprocessing import MultiLabelBinarizer

target = attributes["target"][0]
separator = params["separator"]

df[target] = df[target].apply(lambda x: x.split(separator))
mlb = MultiLabelBinarizer()
df = df.join(pd.DataFrame(mlb.fit_transform(df.pop(target)),
                          columns=mlb.classes_,
                          index=df.index))

return df