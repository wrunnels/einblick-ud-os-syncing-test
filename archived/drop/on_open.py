print(params)

self._axis = params["axis"]

# set how field
if params["threshold"] == 100:
    self._how = "all"
else:
    self._how = "any"

self._relative_threshold = float(params["threshold"]) / 100
self._drop_nan = True if params["nan"] == 1 else False # change to optionValue
self._drop_infinity = True if params["infinity"] == 1 else False # change to optionValue
self._first_batch = True
print(dir(self))
self._columns_to_keep = None

self._subset = None
if not self._subset:
    self._subset = None 

