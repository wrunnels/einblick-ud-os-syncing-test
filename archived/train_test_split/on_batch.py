import numpy as np
p_train = float(params["train_size"]) / 100

df["split_index"] = np.random.choice(["train", "test"], size=len(df), p=[p_train, 1-p_train])
return df