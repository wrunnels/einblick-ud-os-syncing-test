from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_datetime64_any_dtype as is_datetime

def get_values_to_impute(batch):
    """
    Dropna and infinity values to compute mean/mode

    Args:
        batch ([type]): [description]
    """

    with pd.option_context("mode.use_inf_as_null", True):
        self._values_to_impute = dict()
        for col in batch:
            s = batch[col].dropna()
            if (is_numeric_dtype(s) or is_datetime(s)) and (col not in self._mode_attributes):
                self._values_to_impute[col] = s.mean()
            else:
                self._values_to_impute[col] = s.mode()[0]


"""
Impute values (nans and/or infinity) based on user specified constants
or using the mean/mode of each column.
Args:
    batch (pd.DataFrame): [description]

Returns:
    pd.DataFrame: [description]
"""

batch = batches[0]
if self._values_to_impute is None:
    get_values_to_impute(batch)

# impute infinity and nan
if self._impute_infinity and self._impute_nan:
    with pd.option_context("mode.use_inf_as_null", True):
        preprocessed_batch = batch.fillna(self._values_to_impute)
# impute just nans
elif self._impute_nan:
    preprocessed_batch = batch.fillna(self._values_to_impute)
# impute just infinity
elif self._impute_infinity:
    preprocessed_batch = batch.replace(np.infty, self._values_to_impute)
    preprocessed_batch = preprocessed_batch.replace(-np.infty, self._values_to_impute)
# dummy imputation (no effect)
else:
    preprocessed_batch = batch
return preprocessed_batch


