self._impute_nan = params["nan"]
self._impute_infinity = params["infinity"]
self._mode_attributes = set(attributes["mode"])

# dictionary not supported as input yet
# self._values_to_impute = parse_parameter_value(self._description.values_to_impute)
# if not isinstance(self._values_to_impute, dict):
#     raise DavosException("values_to_impute must be a dict")

# no values passed by the client
self._constant = False
self._values_to_impute = None

