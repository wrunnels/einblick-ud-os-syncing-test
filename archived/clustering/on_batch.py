from sklearn.cluster import MiniBatchKMeans
import numpy as np

df = df[attributes["features"]]

self._batch_num += 1
if self._batch_num == 1:
    self._num_clusters = params["num_cluster"]
    self._kmeans = MiniBatchKMeans(n_clusters=self._num_clusters)

df = df.select_dtypes(np.number)
self._kmeans.partial_fit(df)
df["cluster"] = self._kmeans.predict(df)

return df
