# dms2dd import
import pandas as pd
import re

# reverse geocoder imports
import reverse_geocoder as rg
import pycountry

### split column in two
latitude = "latitude"
longitude = "longitude"

df[[latitude, longitude]] = df[self._target].str.split(self._splitter, 1, expand=True)
df = df.drop(self._target, axis=1)

### dms2dd
def dms2dd(s):
    # example: s = """0°51'56.29"S"""
    degrees, minutes, seconds, direction = re.split('[°\'"]+', s)
    dd = float(degrees) + float(minutes)/60 + float(seconds)/(60**2);
    if direction in ('S','W'):
        dd*= -1
    return dd

def convert_row(row):
    res = [dms2dd(col) for col in row]
    return res

df[[latitude, longitude]] = df[[latitude, longitude]].apply(convert_row, axis=1, result_type="expand")

## reverse-geocoder
coordinates =  tuple((e[0], e[1]) for e in df[[latitude, longitude]].values.tolist())

df[["name", "admin1", "admin2", "cc"]] = pd.DataFrame(rg.search(coordinates), columns=["name", "admin1", "admin2", "cc"])
df["country"] = df["cc"].apply(lambda cc: pycountry.countries.get(alpha_2=cc).name)

return df
