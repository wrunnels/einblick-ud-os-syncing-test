import numpy as np

probabilities_columns = [col for col in df.columns if col.find("probabilty_") != -1]

def compute_confidence(row):
    probs = np.sort(row)
    return probs[1] - probs[0]  # 1th probability - 2nd probability

df.insert(loc=0, column="confidence", value=df[probabilities_columns].apply(compute_confidence, axis=1, raw=True))
return df
