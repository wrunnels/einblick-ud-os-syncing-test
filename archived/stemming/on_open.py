from nltk.stem import PorterStemmer
from nltk.corpus import stopwords  
from nltk.tokenize import sent_tokenize, word_tokenize
import nltk
nltk.download('punkt')
nltk.download('stopwords')


self._stemmer = PorterStemmer()
if params["remove_stop_words"] == True:
    self._stop_words = set(stopwords.words('english'))
else:
    self._stop_words = set()

self._tokenizer = word_tokenize