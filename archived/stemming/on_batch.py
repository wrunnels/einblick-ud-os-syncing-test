import string

def stem_sentence(sentence):
    # print(word_tokenize(sentence))
    stemmed_sentence = " ".join([self._stemmer.stem(w) for w in self._tokenizer(sentence) if (not w in self._stop_words and w not in string.punctuation)])
    return stemmed_sentence

def stem_row(row):
    res = [stem_sentence(col) for col in row]
    return res

target = attributes["features"]

if params["keep_original"]:
    new_colname = [col+"_stemmed" for col in target]
    df[new_colname] = df[target].apply(stem_row, axis=1, result_type="expand")
else:
    df[target] = df[target].apply(stem_row, axis=1, result_type="expand")


return df
